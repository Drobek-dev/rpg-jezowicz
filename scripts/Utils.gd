extends Node2D


const SAVE_PATH = "res://savegame.bin"

func delete_save_file():
	if FileAccess.file_exists(SAVE_PATH):
		var dir = DirAccess.open("res://")
		dir.remove("savegame.bin")
		resetGameDataToDefault()
		get_tree().reload_current_scene()
	

func saveGame():
	var file = FileAccess.open(SAVE_PATH, FileAccess.WRITE)
	var data:Dictionary = {
		"playerHP": PlayerState.health,
		"coins": PlayerState.coins,
		"indoor_npc_gone": NpcState.npc_gone,
		"indoor_npc_quest_in_progress":NpcState.quest_in_progress,
		"cac_quest_in_progress": NpcState.cac_quest_in_progress,
		"cac_quest_finished" : NpcState.cac_quest_completed,
		"number_of_cactuses": NpcState.number_of_cactuses,
		"destroyed_cactuses": NpcState.destroyed_cactuses,
		"axe_throwing_enabled": PlayerState.axe_throwing_enabled,
		}
	var jstr = JSON.stringify(data)
	file.store_line(jstr)
	
func resetGameDataToDefault():
	PlayerState.health = 100
	PlayerState.coins = 0
	NpcState.npc_gone = false
	NpcState.quest_in_progress = false
	NpcState.cac_quest_in_progress = false
	NpcState.cac_quest_completed = false
	NpcState.number_of_cactuses = 4
	NpcState.destroyed_cactuses = []
	PlayerState.axe_throwing_enabled = false
	HUD.init_HUD()
	
func loadGame():
	var file = FileAccess.open(SAVE_PATH, FileAccess.READ)
	if FileAccess.file_exists(SAVE_PATH):
		if not file.eof_reached():
			var line = JSON.parse_string(file.get_line())
			if line:
				PlayerState.health = line["playerHP"]
				PlayerState.coins = line["coins"]
				NpcState.npc_gone = line["indoor_npc_gone"]
				NpcState.quest_in_progress = line["indoor_npc_quest_in_progress"]
				NpcState.cac_quest_in_progress = line["cac_quest_in_progress"]
				NpcState.cac_quest_completed = line["cac_quest_finished"]
				NpcState.number_of_cactuses = line["number_of_cactuses"]
				NpcState.destroyed_cactuses = line["destroyed_cactuses"]
				PlayerState.axe_throwing_enabled = line["axe_throwing_enabled"]
