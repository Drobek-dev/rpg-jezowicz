
extends Area2D


@export var give_quest_message: String = "All my father cares about are the bloody cactuses!\nOnce I have some money I will leave this island and become and actress in Hollywood!\nWill you help me? \n\n\n[Press Y to accept the quest]"
@export var remind_quest_message: String = "Once I have the gold I am gonna leave this house!\n\n\n[Press G to give coins]"
@export var not_enough_gold_quest_message: String = "Sorry, I need it all at once."
@export var leaving_quest_message: String = "Thank you!\nI am gonna start packing..."


func set_label_text():
	if !NpcState.quest_in_progress:
		$CanvasLayer/TextureRect/Label.text = give_quest_message
	elif NpcState.npc_gone:
		$CanvasLayer/TextureRect/Label.text = leaving_quest_message
	else:
		$CanvasLayer/TextureRect/Label.text = remind_quest_message

func _ready():
	if(NpcState.npc_gone):
		queue_free()
	set_label_text()
	
	
func play_tarot_card():
	PlayerState.video_playing = true
	var player =get_tree().get_current_scene().get_node("Camera2D/CanvasLayer/VideoStreamPlayer") 
	player.play()
	await player.finished
	PlayerState.video_playing = false
	
	
func _process(delta):
	if NpcState.npc_gone:
		return
	
	if Input.is_key_pressed(KEY_Y) && $CanvasLayer/TextureRect.visible:
		$CanvasLayer/TextureRect/Label.text = remind_quest_message
		NpcState.quest_in_progress = true
		Utils.saveGame()
		
	if Input.is_key_pressed(KEY_G) && NpcState.quest_in_progress:
		print(PlayerState.coins)
		if(PlayerState.coins >= NpcState.required_gold):
			NpcState.npc_gone = true
			PlayerState.add_coins(-NpcState.required_gold) 
			$CanvasLayer/TextureRect/Label.text = leaving_quest_message
			play_tarot_card()
		else:
			$CanvasLayer/TextureRect/Label.text = not_enough_gold_quest_message	
		Utils.saveGame()


func _on_area_entered(area):
	if area.is_in_group("Player"):
		print("player entered")
		$AnimationPlayer.play("Appear")


func _on_area_exited(area):
		if area.is_in_group("Player"):
			$AnimationPlayer.play_backwards("Appear")
		set_label_text()
