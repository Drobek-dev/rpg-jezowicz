extends Camera2D
@export var follow_target: Node2D
@export var speed := 2.0;

var shake_duration := 0.2
var shake_scale := 4
var shake_times := 10

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(delta):
	position = lerp(position,follow_target.position, delta * speed)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func shake():
	var tween = create_tween()
	for i in range(shake_times -1):
		var newpos = Vector2(
			randf_range(-shake_scale,shake_scale),
			randf_range(-shake_scale,shake_scale)
		)
		tween.tween_property(self, "offset", newpos, shake_duration / shake_times)
	tween.tween_property(self, "offset", Vector2.ZERO, shake_duration/ shake_times)
