extends Node2D


# Called when the node enters the scene tree for the first time.
func _ready():
	var children = get_children()
	var num_of_cactuses = NpcState.number_of_cactuses
	for ch in children:
		if ch.name in NpcState.destroyed_cactuses:
			ch.queue_free()
		
		
