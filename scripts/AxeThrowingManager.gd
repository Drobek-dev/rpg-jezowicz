extends Node2D

@onready var AXE = preload("res://scenes/ThrowingAxe.tscn")



func throw_axe(oriented_left:bool, position: Vector2):
	var world_node = get_tree().get_current_scene().get_node("/root/World")
	if(world_node == null): 
		return
	var axe = AXE.instantiate()
	axe.oriented_left = oriented_left
	axe.position = position
	world_node.add_child(axe)
	$AxeThrowFreq.start()
