extends Area2D


var health: float = 1.0
var axes = []
var health_tween:Tween
var axe_damage = 0.25

func _ready():
	$ProgressBarGreen.hide()
	$ProgressBar.hide()

func on_collision(axe):
	$ProgressBar.show()
	$ProgressBarGreen.show()
	$DamageTextManager.show_value(axe_damage*100)
	take_damage_form_axe(axe)
	die_if_health_depleted()
	
func take_damage_form_axe(axe):
	axes.append(axe)
	health -= axe_damage
	$Sprite2D.modulate = Color(health,health,health)
	update_health(health)	
	
func die_if_health_depleted():
	if(health <= 0):
		NpcState.number_of_cactuses -= 1
		NpcState.destroyed_cactuses.append(self.name)
		
		Utils.saveGame()
		queue_free()
		for a in axes:
			if a != null:
				a.queue_free()
				
func update_health(value):
	$ProgressBarGreen.value = value
	if health_tween != null && health_tween.is_running():
		health_tween.stop()
		
	health_tween = create_tween()
	health_tween.tween_property($ProgressBar, "value", value,1.0)
