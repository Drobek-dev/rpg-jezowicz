extends Node


const MAX_HEALTH = 100

@onready var health = 100
@onready var coins: int = 0
var target_location:Vector2
var game_launched:bool = true
var axe_throwing_enabled = false
const target_outside_location:= Vector2(472, 377)
const target_indoor_location:= Vector2(502,457) 
var video_playing = false

func _ready():
	if game_launched:
		Utils.loadGame()
		game_launched = false
		
	target_location = target_outside_location
	HUD.update_coins(coins,coins)
func increase_health(value: int):
	health = clamp(health + value, 0, MAX_HEALTH)
	HUD.update_health(health)

func decrease_health(value: int):
	health = clamp(health - value, 0, MAX_HEALTH)
	HUD.update_health(health)
	
func add_coins(value: int):
	coins += value
	HUD.update_coins(coins - value, coins)
