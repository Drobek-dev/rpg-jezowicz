extends Area2D


@export var next_scene: String = "res://scenes/House.tscn"
@export var message: String = "Press SPACE to enter."
@export var target_location:Vector2


var is_active = false


func _ready():
	$CanvasLayer/TextureRect2/Label.text = message
	SceneTransition.fade_in()

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if(next_scene.contains("House")):
			PlayerState.target_location = PlayerState.target_indoor_location
		else:
			PlayerState.target_location = PlayerState.target_outside_location
			
		SceneTransition.fade_out();
		await SceneTransition.animation.animation_finished
		get_tree().change_scene_to_file(next_scene)
		$AnimationPlayer.play_backwards("Appear")


func _on_Teleport_area_entered(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play("Appear")
		is_active = true


func _on_Teleport_area_exited(area):
	if area.is_in_group("Player"):
		$AnimationPlayer.play_backwards("Appear")
		is_active = false
