extends Node2D



@onready var FCT = preload("res://scenes/DamageText.tscn")

@export var travel = Vector2(0, -80)
@export var duration = 2
@export var spread = PI/2

func show_value(value):
	var fct = FCT.instantiate()
	add_child(fct)
	fct.show_value(str(value), travel, duration, spread)
