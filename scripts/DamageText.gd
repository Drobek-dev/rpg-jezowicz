extends Label

var tweenPos:Tween
var tweenAlpha:Tween

func show_value(value, travel, duration, spread):
	text = value
	var movement = travel.rotated(randf_range(-spread/4, spread/4))
	#pivot_offset = size / 2
	
	tweenPos = create_tween()
	tweenPos.tween_property(self, "position", position + movement,duration).set_ease(Tween.EASE_OUT)
	tweenAlpha = create_tween()
	tweenAlpha.tween_property(self,"modulate:a",0,duration).set_ease(Tween.EASE_OUT)
	await tweenPos.finished
	await tweenAlpha.finished
	queue_free()
