extends CanvasLayer

@onready var health_tween: Tween
var coins_tween: Tween


func _ready():
	init_HUD()

func init_HUD():
	$CanvasLayer/ProgressBar.value = PlayerState.health
	$CanvasLayer/ProgressBarYellow.value = PlayerState.health


func update_coins(old_value:float, value:float):
	Utils.saveGame()
	if coins_tween != null && coins_tween.is_running():
		coins_tween.stop()
		
	coins_tween = create_tween()
	coins_tween.tween_method(func(value): $CanvasLayer/Label.text = "Coins: " + str(ceil(value)), old_value,value,1.0).set_trans(Tween.TRANS_SINE)

	
func update_health(value):
	$CanvasLayer/ProgressBarYellow.value = value
	Utils.saveGame()
	if health_tween != null && health_tween.is_running():
		health_tween.stop()
		
	health_tween = create_tween()
	health_tween.tween_property($CanvasLayer/ProgressBar, "value", value,1.0)

