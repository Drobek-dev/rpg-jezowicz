extends Area2D


@export var scale_curve: Curve
var random_offset = randi_range(0.0, 2)
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var time = Time.get_ticks_msec() + random_offset
	var x = (time % 1000) / 1000.0

	var scale_change = Vector2.ONE * scale_curve.sample(x)
	$Sprite2D.scale = scale_change
