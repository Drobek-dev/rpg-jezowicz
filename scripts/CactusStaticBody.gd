extends StaticBody2D

func on_collision():
	get_viewport().get_camera_2d().shake()
	PlayerState.decrease_health(10)
