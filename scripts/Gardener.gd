
extends Area2D


var give_quest_message: String = "My field is infested with cactuses. \nI will reward you with 15 gold if you aid me. Whaddaya say? \n\n\n[Press Y to accept the quest]"
var assign_quest_message: String = "Use these axes to clean my field!\n\n\n[Press Q to throw axe]"
var reward_quest_message: String = "Thank you!\n Here is your gold..."
var end_quest_message: String = "Thank you!"



func set_label_text():
	if !NpcState.cac_quest_in_progress:
		$CanvasLayer/TextureRect/Label.text = give_quest_message
	elif NpcState.cac_quest_completed:
		$CanvasLayer/TextureRect/Label.text = end_quest_message
	else:
		$CanvasLayer/TextureRect/Label.text = assign_quest_message

func _ready():
	
	set_label_text()
	
	
	
func _process(delta):
	if NpcState.cac_quest_completed || not $CanvasLayer/TextureRect.modulate.a > 0:
		return
	
	if Input.is_key_pressed(KEY_Y) :
		$CanvasLayer/TextureRect/Label.text = assign_quest_message
		NpcState.cac_quest_in_progress = true
		PlayerState.axe_throwing_enabled = true
		Utils.saveGame()
		
	elif NpcState.cac_quest_in_progress:
		if(NpcState.number_of_cactuses == 0):
			$CanvasLayer/TextureRect/Label.text = reward_quest_message
			PlayerState.add_coins(15)
			NpcState.cac_quest_completed= true
			Utils.saveGame()
			
		


func _on_area_entered(area):
	if area.is_in_group("Player"):
		print("player entered")
		$AnimationPlayer.play("Appear")
		set_label_text()


func _on_area_exited(area):
		if area.is_in_group("Player"):
			$AnimationPlayer.play_backwards("Appear")
		
