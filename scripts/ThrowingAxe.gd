extends Area2D

var oriented_left = false
var rotation_speed = 16
var movement_speed = 64
var moving = true

@onready var lifeTimer = $AxeLifeTimeTimer
# Called when the node enters the scene tree for the first time.
func _ready():
	lifeTimer.start()
	await lifeTimer.timeout
	if moving:
		queue_free()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if not moving:
		return
	$Sprite2D.flip_h = oriented_left
	$Sprite2D.rotate(rotation_speed * delta * (-1 if oriented_left else 1))
	position.x += delta * movement_speed * (-1 if oriented_left else 1)


func _on_area_entered(area):
	if area.is_in_group("Cactuses"):
		area.on_collision(self)
		moving = false
		$Sprite2D.rotation = -1 if oriented_left else 1
		$AxeDisappearTimer.start()
		await $AxeDisappearTimer.timeout
		queue_free()
