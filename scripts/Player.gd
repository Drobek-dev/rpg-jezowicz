extends Area2D


@export var speed: float = 4
signal moved

const TILE_SIZE = 16

const inputs = {
	"Left": Vector2.LEFT,
	"Right": Vector2.RIGHT,
	"Up": Vector2.UP,
	"Down": Vector2.DOWN
}

var last_dir = Vector2.ZERO
var last_action = ""

var move_tween:Tween;
var target_pos:Vector2 
var oriented_left = false


func _ready():
	
	
	# align position to the middle of a tile
	position.x = int(position.x / TILE_SIZE) * TILE_SIZE
	position.y = int(position.y / TILE_SIZE) * TILE_SIZE
	position += Vector2.ONE * TILE_SIZE/2
	# set timer interval according to the speed
	$MoveTimer.wait_time = 1.0/speed
	
	if PlayerState.target_location != Vector2.ZERO:
		position = PlayerState.target_location
		target_pos = position

func _process(delta):
	if PlayerState.health <= 0:
		Utils.delete_save_file()
		

func _physics_process(delta):
	if target_pos.distance_squared_to(position) < 0.2:
		if move_tween != null:
			move_tween.stop()
		position = target_pos
	

func _unhandled_input(event):
	
	for action in inputs:
		if event.is_action_pressed(action) && target_pos == position:
			var dir = inputs[action]
			oriented_left = true if dir == Vector2.LEFT else false
			if move_tile(dir):
				# repeat the action in fixed intervals, if it is still pressed
				last_action = action
				last_dir = dir
				$MoveTimer.start()

	if PlayerState.axe_throwing_enabled && event.as_text() == "Q" && $AxeThrowingManager/AxeThrowFreq.is_stopped():
		print(event.pressed)
		$AxeThrowingManager.throw_axe(oriented_left, position)


		
			

func move_tile(direction: Vector2):
	if PlayerState.video_playing:
		return
	$RayCast2D.target_position = direction * TILE_SIZE 
	$RayCast2D.force_raycast_update()
	if !$RayCast2D.is_colliding():
		target_pos += direction * TILE_SIZE
		move_tween = create_tween()
		move_tween.tween_property(self, "position", position + direction * TILE_SIZE, 1.0/speed).set_trans(Tween.TRANS_CUBIC)
		
		moved.emit()
		return true
	var other = $RayCast2D.get_collider()
	print(other.name)
	
	if other.has_method("on_collision"):
		other.on_collision()
		modulate_color()
		$DamageTextManager.show_value("10")
	return false
	
func modulate_color():
	self.modulate = Color(1.0,0.0,0.0)
	$HurtTimer.start()
	await $HurtTimer.timeout
	self.modulate = Color(1,1,1)
	

func _on_MoveTimer_timeout():
	if Input.is_action_pressed(last_action):
		if move_tile(last_dir):  # do the same move as the last time
			return
	# reset
	last_action = ""
	last_dir = Vector2.ZERO
	$MoveTimer.stop()

func get_hurt(value):
	PlayerState.decrease_health(value)

func collect_coins(value):
	PlayerState.add_coins(value)
	



func _on_area_entered(area):
	var area_name = area.name
	print("area entered")
	if not area_name.begins_with("Coin"):
		return
	if area_name.begins_with("CoinsS"):
		print("small entered")
		collect_coins(3)
	elif area_name.begins_with("CoinsM"):
		print("medium entered")
		collect_coins(6)
	elif area_name.begins_with("CoinsL"):
		print("large entered")
		collect_coins(9)
		
	area.queue_free()
